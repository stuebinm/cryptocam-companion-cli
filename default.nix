{ pkgs ? import <nixpkgs> {}, ... }:


let
  naerskSource = import (pkgs.fetchFromGitHub {
    repo = "naersk";
    owner = "nix-community";
    rev = "6944160c19cb591eb85bbf9b2f2768a935623ed3";
    sha256 = "sha256-9o2OGQqu4xyLZP9K6kNe1pTHnyPz0Wr3raGYnr9AIgY=";
  });
  naersk = pkgs.callPackage naerskSource {};
in naersk.buildPackage {
  name = "cryptocam-cli";
  version = "0.1.1";
  src = ./.;
  # our only external dependency is ffmpeg; pkg-config
  # is just needed so that cargo can find it
  buildInputs = with pkgs; [ ffmpeg pkg-config ];
}
